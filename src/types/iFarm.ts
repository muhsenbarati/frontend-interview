export type iFarm = {
  id: number;
  city: string;
  country: string;
  name: string;
};