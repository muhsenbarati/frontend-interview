export type iOrder = {
  id: number;
  farmId: number;
  date: string;
  quantity: number;
  status?: string;
};
