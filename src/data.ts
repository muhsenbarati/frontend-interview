
export const farmData = [
  {
    id: 1,
    city: "Amsterdam",
    country: "Netherlands",
    name: "Mooie Boerderij",
  }, {
    id: 2,
    city: "Paris",
    country: "France",
    name: "Belle Ferme",
  }, {
    id: 3,
    city: "Berlin",
    country: "Germany",
    name: "Schöner Bauernhof",
  }, {
    id: 4,
    city: "Rome",
    country: "Italy",
    name: "Bella Fattoria",
  }, {
    id: 5,
    city: "Barcelona",
    country: "Spain",
    name: "Granja Hermosa",
  }, {
    id: 6,
    city: "London",
    country: "United Kingdom",
    name: "Beautiful Farm",
    contamination: true,
  }, {
    id: 7,
    city: "Lisbon",
    country: "Portugal",
    name: "Fazenda Bonita",
  }, {
    id: 8,
    city: "Lviv",
    country: "Ukraine",
    name: "Гарна ферма",
  }, {
    id: 9,
    city: "Prague",
    country: "Czech Republic",
    name: "Krásná farma",
  }, {
    id: 10,
    city: "Athens",
    country: "Greece",
    name: "Όμορφη φάρμα",
  }
]

export const orderData = [
  {
    id: 1,
    farmId: 1,
    date: "2021-03-15",
    quantity: 1020,
  }, {
    id: 2,
    farmId: 3,
    date: "2021-01-02",
    quantity: 2200,
  }, {
    id: 3,
    farmId: 2,
    date: "2021-02-25",
    quantity: 800,
  }, {
    id: 4,
    farmId: 4,
    date: "2023-03-15",
    quantity: 1200,
  }, {
    id: 5,
    farmId: 5,
    date: "2020-07-11",
    quantity: 1500,
  }, {
    id: 6,
    farmId: 5,
    date: "2021-06-13",
    quantity: 2000,
  }, {
    id: 7,
    farmId: 7,
    date: "2022-08-22",
    quantity: 1000,
  }, {
    id: 8,
    farmId: 7,
    date: "2023-10-25",
    quantity: 1500,
  }, {
    id: 9,
    farmId: 9,
    date: "2021-11-05",
    quantity: 1800,
  }, {
    id: 10,
    farmId: 10,
    date: "2020-12-09",
    quantity: 2200,
    status: 'received',
  }, {
    id: 11,
    farmId: 6,
    date: "2022-01-02",
    quantity: 1400,
  }, {
    id: 12,
    farmId: 8,
    date: "2023-03-15",
    quantity: 1200,
  }, {
    id: 13,
    farmId: 8,
    date: "2021-06-13",
    quantity: 2000,
  }, {
    id: 14,
    farmId: 6,
    date: "2022-08-22",
    quantity: 1000,
  }, {
    id: 15,
    farmId: 10,
    date: "2023-10-25",
    quantity: 1500,
  }, {
    id: 16,
    farmId: 9,
    date: "2021-11-05",
    quantity: 1800,
  }, {
    id: 17,
    farmId: 4,
    date: "2020-12-09",
    quantity: 2200,
  }, {
    id: 18,
    farmId: 3,
    date: "2022-01-02",
    quantity: 1400,
  }
]
